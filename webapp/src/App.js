import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import './CSS/App.css';
import Header from "./Layout/Header";
import {Button, Col, Grid, Row} from "react-bootstrap";
import axios from "axios";
import QuestionPage from "./QuestionPage";
import EditPage from "./EditPage";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            question: props.question,
            answer1: props.answer1,
            answer2: props.answer2,
            answer3: props.answer3,
            correctAnswer: props.correctAnswer,
            newQuestion: props.newQuestion,
            currentQuestionNr: props.currentQuestionNr,
            wrongAnswersGiven: props.wrongAnswersGiven,
            correctAnswersGiven: props.correctAnswersGiven,
            amountOfQuestions: this.getQuestionAmount(),
        };
        this.handleKeys = this.handleKeys.bind(this);
    }

    loadQuestion() {
        this.setState({
            newQuestion: true,
        })
        axios(
            {
                headers: {

                    'Access-Control-Allow-Origin': '*',
                    'cross-origin': 'true',
                },
                method: "GET",
                crossDomain: true,
                url: "https://localhost:62652/question",
            }
        ).then((response) => {
            this.setState({
                question: response.data.question,
                answer1: response.data.answer1,
                answer2: response.data.answer2,
                answer3: response.data.answer3,
                correctAnswer: response.data.correctAnswer,
            });
        });
    }

    enterNewQuestion() {
        document.removeEventListener("keyup", this.handleKeys);
        ReactDOM.render(<QuestionPage answer1={this.state.answer1} answer2={this.state.answer2}
                                      answer3={this.state.answer3} question={this.state.question}
                                      currentQuestionNr={this.state.currentQuestionNr}
                                      correctAnswer={this.state.correctAnswer}
                                      correctAnswersGiven={this.state.correctAnswersGiven}
                                      wrongAnswersGiven={this.state.wrongAnswersGiven}
                                      newQuestion={this.state.newQuestion}/>, document.getElementById('root'))
    }

    editQuestion() {
        document.removeEventListener("keyup", this.handleKeys);
        ReactDOM.render(<EditPage answer1={this.state.answer1} answer2={this.state.answer2} answer3={this.state.answer3}
                                  question={this.state.question} currentQuestionNr={this.state.currentQuestionNr}
                                  correctAnswer={this.state.correctAnswer}
                                  correctAnswersGiven={this.state.correctAnswersGiven}
                                  wrongAnswersGiven={this.state.wrongAnswersGiven}
                                  newQuestion={this.state.newQuestion}/>, document.getElementById('root'));
    }

    answerSelected(answerNr) {
        if (this.state.newQuestion) {
            axios(
                {
                    headers: {
                        'Access-Control-Allow-Origin': '*',
                        'cross-origin': 'true',
                    },
                    method: "GET",
                    crossDomain: true,
                    url: "https://localhost:62652/answer",
                    params: {
                        answered: (this.state.correctAnswer === answerNr)
                    }
                }
            ).then((response) => {
                this.setState({
                    wrongAnswersGiven: response.data.wrongAnswersGiven,
                    correctAnswersGiven: response.data.correctAnswersGiven,
                    currentQuestionNr: response.data.currentQuestionNr,
                    newQuestion: false,
                })
            });

        }
        else {
            this.loadQuestion();
        }
        window.removeEventListener("keyup", this.handleKeys);
    }

    getQuestionAmount() {
        axios(
            {
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'cross-origin': 'true',
                },
                method: "GET",
                crossDomain: true,
                url: "https://localhost:62652/amountOfQuestions",
            }
        ).then((response) => {
            this.setState({
                amountOfQuestions: response.data.amountOfQuestions,
            })
        });
    }

    handleKeys(event) {
        if (this.state.newQuestion) {

            switch (event.keyCode) {
                case 49:
                    this.answerSelected(1);
                    break;
                case 50:
                    this.answerSelected(2);
                    break;
                case 51 :
                    this.answerSelected(3);
                    break;
                case 116:
                    break;
                case 123:
                    break;
                default:
                    alert("Antworten werden mit 1, 2 oder 3 ausgewählt");
            }
        } else {
            if (event.keyCode === 52) {
                this.loadQuestion();

            } else if (event.keyCode === 116 || event.keyCode === 123) {

            } else {
                alert("Mit \"4\" wird die nächste Frage geladen");
            }
        }
    }

    render() {
        return (
            <div className="App">
                <Header/>
                {document.addEventListener("keyup", this.handleKeys)}
                <Grid className={"buttonGrid"}>
                    <div id="frage">
                        <Row>
                            <Col xs={12} md={12}>
                                {this.state.question ? <div> {this.state.question} </div> : <div> Hallo </div>}
                            </Col>
                        </Row>
                    </div>

                    <Row>
                        <Col xs={12} md={12}>
                            {
                                (this.state.correctAnswer === 1) && !(this.state.newQuestion) ?
                                    <Button onClick={() => this.answerSelected(1)} id={"1"} block bsStyle="success"
                                            bsSize="large">
                                        <h3>
                                            {this.state.answer1}
                                        </h3>
                                    </Button>
                                    :
                                    <Button onClick={() => this.answerSelected(1)} id={"1"} block bsStyle="primary"
                                            bsSize="large">
                                        <h3>
                                            {this.state.answer1}
                                        </h3>
                                    </Button>
                            }
                        </Col>
                    </Row>

                    <Row>
                        <Col xs={12} md={12}>
                            {
                                (this.state.correctAnswer === 2) && !(this.state.newQuestion) ?
                                    <Button onClick={() => this.answerSelected(2)} id={"2"} block bsStyle="success"
                                            bsSize="large">
                                        <h3>
                                            {this.state.answer2}
                                        </h3>
                                    </Button>
                                    :
                                    <Button onClick={() => this.answerSelected(2)} id={"2"} block bsStyle="primary"
                                            bsSize="large">
                                        <h3>
                                            {this.state.answer2}
                                        </h3>
                                    </Button>
                            }
                        </Col>
                    </Row>

                    <Row>
                        <Col xs={12} md={12}>
                            {
                                (this.state.correctAnswer === 3) && !(this.state.newQuestion) ?
                                    <Button onClick={() => this.answerSelected(3)} id={"3"} block bsStyle="success"
                                            bsSize="large">
                                        <h3>
                                            {this.state.answer3}
                                        </h3>
                                    </Button>
                                    :
                                    <Button onClick={() => this.answerSelected(3)} id={"3"} block bsStyle="primary"
                                            bsSize="large">
                                        <h3>
                                            {this.state.answer3}
                                        </h3>
                                    </Button>
                            }
                        </Col>
                    </Row>
                </Grid>

                <div>
                    <h3>
                        <span className="label label-success">
                            {this.state.correctAnswersGiven}
                        </span>
                        <span className="label label-danger">
                            {this.state.wrongAnswersGiven}
                        </span>
                        <span className="label label-primary">
                            {this.state.currentQuestionNr}/{this.state.amountOfQuestions}
                        </span>
                    </h3>
                </div>

                <div>
                    <Grid id={"navigatorGrid"}>
                        <Row>
                            <Col xs={12} md={12}>
                                <Button id={"addNewQuestion"} bsStyle={"info"} onClick={() => this.enterNewQuestion()}>
                                    <h5>
                                        Neue Frage einreichen
                                    </h5>
                                </Button>
                            </Col>
                        </Row>

                        <Row>
                            <Col xs={12} md={12}>
                                {
                                    (this.state.correctAnswer !== 0) ?
                                        <Button id={"editing"} bsStyle={"warning"} onClick={() => this.editQuestion()}>
                                            <h5>
                                                &nbsp;Diese Frage editieren&nbsp;
                                            </h5>
                                        </Button>
                                        :
                                        <h5>
                                        </h5>
                                }
                            </Col>
                        </Row>
                    </Grid>
                </div>
            </div>
        );
    }
}

export default App;