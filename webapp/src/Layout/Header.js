import React, {Component} from 'react';
import './Header.css';
import schmalzLogo from './schmalzLogoKlein.png';
import {Col, Grid, Row} from "react-bootstrap";


export default class Header extends Component {
    render() {
        return (
            <div className="Header">
                <Grid>
                    <Row className="show-grid">
                        <Col xs={12} md={4} >
                            <img id={"logo"} src={schmalzLogo} alt={"Schmalzlogo"}/>
                        </Col>
                        <Col xs={12} md={4} >
                            <h1 id={"title"}>WissensApp</h1>
                        </Col>
                        <Col  md={4}>
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}
