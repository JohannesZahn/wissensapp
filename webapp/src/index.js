import React from 'react';
import ReactDOM from 'react-dom';
import './CSS/index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App question={"Herzlich Willkommen Antwort auswählen mit 1, 2 oder 3"} answer1={"Bestätigen mit 4"} answer2={"Quiz starten"}
                     answer3={"Quiz starten"}
                     correctAnswer={0} currentQuestionNr={0} wrongAnswersGiven={0}
                     correctAnswersGiven={0} newQuestion={false}/>, document.getElementById('root'));
registerServiceWorker();