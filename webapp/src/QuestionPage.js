import React, {Component} from 'react';
import App from "./App";
import './CSS/App.css';
import Header from "./Layout/Header";
import './CSS/QuestionPage.css';
import {Button, Col, Grid, Row} from "react-bootstrap";
import axios from "axios";
import ReactDOM from "react-dom";

class QuestionPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            question: '',
            answer1: '',
            answer2: '',
            answer3: '',
            correctAnswer: '',

            oldQuestion: props.question,
            oldAnswer1: props.answer1,
            oldAnswer2: props.answer2,
            oldAnswer3: props.answer3,
            oldCorrectAnswer: props.correctAnswer,
            currentQuestionNr: props.currentQuestionNr,
            correctAnswersGiven: props.correctAnswersGiven,
            wrongAnswersGiven: props.wrongAnswersGiven,
            newQuestion: props.newQuestion,
        };
        this.handleChangeQuestion = this.handleChangeQuestion.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeAnswer1 = this.handleChangeAnswer1.bind(this);
        this.handleChangeAnswer2 = this.handleChangeAnswer2.bind(this);
        this.handleChangeAnswer3 = this.handleChangeAnswer3.bind(this);
        this.handleChangeCorrectAnswer = this.handleChangeCorrectAnswer.bind(this);
    }

    returnToQuiz() {
        ReactDOM.render(<App question={this.state.oldQuestion} answer1={this.state.oldAnswer1}
                             answer2={this.state.oldAnswer2} answer3={this.state.oldAnswer3}
                             correctAnswer={this.state.oldCorrectAnswer}
                             correctAnswersGiven={this.state.correctAnswersGiven}
                             wrongAnswersGiven={this.state.wrongAnswersGiven}
                             newQuestion={this.state.newQuestion}
                             currentQuestionNr={this.state.currentQuestionNr}/>, document.getElementById('root'))
    }

    handleChangeQuestion(event) {
        this.setState({question: event.target.value});
    }

    handleChangeAnswer1(event) {
        this.setState({answer1: event.target.value});
    }

    handleChangeAnswer2(event) {
        this.setState({answer2: event.target.value});
    }

    handleChangeAnswer3(event) {
        this.setState({answer3: event.target.value});
    }

    handleChangeCorrectAnswer(event) {
        this.setState({correctAnswer: event.target.value});
    }

    handleSubmit(event) {
        this.submitQuestion();
        event.preventDefault();
        alert("Frage erfolgreich hinzugefügt");
    }

    submitQuestion() {
        axios(
            {
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'cross-origin': 'true',
                },
                method: "GET",
                crossDomain: true,
                url: "https://localhost:62652/newQuestion",
                params: {
                    question: this.state.question,
                    answer1: this.state.answer1,
                    answer2: this.state.answer2,
                    answer3: this.state.answer3,
                    correctAnswer: this.state.correctAnswer,
                }
            }
        ).then((response) => {
             })
        ;
    }

    render() {
        return (
            <div className="QuestionPage">
                <Header/>

                <form onSubmit={this.handleSubmit} id={"addForm"}>
                    <Grid id={"formGrid"}>
                        <Row>
                            <Col xs={12} md={12}>
                                <h1>
                                    Hier können Sie Ihre neue Frage eingeben
                                </h1>
                            </Col>
                        </Row>

                        <Row>
                            <Col xs={12} md={12}>
                                <div className={"input-group"}>
                                    <span className={"input-group-addon"} id={"question"}>Frage </span>
                                    <input type={"text"} className={"form-control"} aria-describedby="question"
                                           value={this.state.question} onChange={this.handleChangeQuestion}/>
                                </div>
                            </Col>
                        </Row>

                        <Row>
                            <Col xs={12} md={12}>
                                <div className={"input-group"}>
                                    <span className={"input-group-addon"} id={"answer1"}>Antwort 1 </span>
                                    <input type={"text"} className={"form-control"} aria-describedby="answer1"
                                           value={this.state.answer1} onChange={this.handleChangeAnswer1}/>
                                </div>
                            </Col>
                        </Row>

                        <Row>
                            <Col xs={12} md={12}>
                                <div className={"input-group"}>
                                    <span className={"input-group-addon"} id={"answer2"}>Antwort 2 </span>
                                    <input type={"text"} className={"form-control"} aria-describedby="answer2"
                                           value={this.state.answer2} onChange={this.handleChangeAnswer2}/>
                                </div>
                            </Col>
                        </Row>

                        <Row>
                            <Col xs={12} md={12}>
                                <div className={"input-group"}>
                                    <span className={"input-group-addon"} id={"answer3"}>Antwort 3 </span>
                                    <input type={"text"} className={"form-control"} aria-describedby="answer3"
                                           value={this.state.answer3} onChange={this.handleChangeAnswer3}/>
                                </div>
                            </Col>
                        </Row>

                        <Row>
                            <Col xs={12} md={12}>
                                <div className={"input-group"}>
                                    <span className={"input-group-addon"} id={"correctAnswer"}>Korrekte Antwort </span>
                                    <input type={"text"} className={"form-control"} placeholder={"1, 2 oder 3"}
                                           aria-describedby="correctAnswer" value={this.state.correctAnswer}
                                           onChange={this.handleChangeCorrectAnswer}/>
                                </div>
                            </Col>
                        </Row>

                        <Row>
                            <Col xs={12} md={12}>
                                <input type={"submit"} id={"submit"} value={"Hinzufügen"}/>
                            </Col>
                        </Row>

                    </Grid>
                </form>
                <h3>
                    <div>
                        <Button id={"returnToQuiz"} bsStyle={"primary"} onClick={() => this.returnToQuiz()}
                                bsSize={"large"}>
                            Kehre zum Quiz zurück
                        </Button>
                    </div>
                </h3>
            </div>
        );
    }
}

export default QuestionPage;