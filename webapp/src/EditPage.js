import React from "react";
import axios from "axios";
import ReactDOM from 'react-dom';
import {Button, Col, Grid, Row} from "react-bootstrap";
import Header from "./Layout/Header";
import App from "./App"
import './CSS/App.css';
import './CSS/QuestionPage.css';

class EditPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            correctAnswersGiven: props.correctAnswersGiven,
            wrongAnswersGiven: props.wrongAnswersGiven,
            question: props.question,
            answer1: props.answer1,
            answer2: props.answer2,
            answer3: props.answer3,
            correctAnswer: props.correctAnswer,
            questionToEdit: props.currentQuestionNr,
            editMode: true,
            newQuestion: props.newQuestion,

            oldQuestion: props.question,
            oldAnswer1: props.answer1,
            oldAnswer2: props.answer2,
            oldAnswer3: props.answer3,
            oldCorrectAnswer: props.correctAnswer,
        };

        this.handleChangeQuestion = this.handleChangeQuestion.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeAnswer1 = this.handleChangeAnswer1.bind(this);
        this.handleChangeAnswer2 = this.handleChangeAnswer2.bind(this);
        this.handleChangeAnswer3 = this.handleChangeAnswer3.bind(this);
        this.handleChangeCorrectAnswer = this.handleChangeCorrectAnswer.bind(this);
        this.handleChangeQuestionToDelete = this.handleChangeQuestionToDelete.bind(this);
    }

    returnToQuiz() {
        ReactDOM.render(<App answer1={this.state.oldAnswer1} answer2={this.state.oldAnswer2} answer3={this.state.oldAnswer3}
                             question={this.state.oldQuestion} currentQuestionNr={this.state.questionToEdit}
                             correctAnswer={this.state.oldCorrectAnswer} wrongAnswersGiven={this.state.wrongAnswersGiven}
                             correctAnswersGiven={this.state.correctAnswersGiven}
                             newQuestion={this.state.newQuestion}/>, document.getElementById('root'))
    }

    handleChangeQuestionToDelete(event) {
        this.setState({questionToEdit: event.target.value})
    }

    handleChangeQuestion(event) {
        this.setState({question: event.target.value});
    }

    handleChangeAnswer1(event) {
        this.setState({answer1: event.target.value});
    }

    handleChangeAnswer2(event) {
        this.setState({answer2: event.target.value});
    }

    handleChangeAnswer3(event) {
        this.setState({answer3: event.target.value});
    }

    handleChangeCorrectAnswer(event) {
        this.setState({correctAnswer: event.target.value});
    }

    handleSubmit(event) {

        if (this.state.editMode) {
            alert("The question is now being edited");
            this.editQuestion();
        }
        else {
            alert("The question is now being deleted");
            this.deleteQuestion();
        }
        event.preventDefault();
    }

    deleteQuestion() {
       axios(
            {
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'cross-origin': 'true',
                },
                method: "GET",
                crossDomain: true,
                url: "https://localhost:62652/questionDelete",
                params: {
                    questionToEdit: this.state.questionToEdit,
                }
            }
        ).then((response) => {
            this.setState({
                question: response.data.question,
                answer1: response.data.answer1,
                answer2: response.data.answer2,
                answer3: response.data.answer3,
                correctAnswer: response.data.correctAnswer,
            });
        });
    }

    editQuestion() {
        this.setState({
            oldQuestion: this.state.question,
            oldAnswer1: this.state.answer1,
            oldAnswer2: this.state.answer2,
            oldAnswer3: this.state.answer3,
            oldCorrectAnswer: this.state.correctAnswer
        });

        axios(
            {
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'cross-origin': 'true',
                },
                method: "GET",
                crossDomain: true,
                url: "https://localhost:62652/questionEdit",
                params: {
                    questionToEdit: this.state.questionToEdit,
                    question: this.state.question,
                    answer1: this.state.answer1,
                    answer2: this.state.answer2,
                    answer3: this.state.answer3,
                    correctAnswer: this.state.correctAnswer,
                }
            }
        ).then((response) => {
            this.setState({
                question: response.data.question,
                answer1: response.data.answer1,
                answer2: response.data.answer2,
                answer3: response.data.answer3,
                correctAnswer: response.data.correctAnswer,
            });
        });
    }

    toggleMode() {
        if (this.state.editMode) {
            this.setState({
                editMode: false,
            })
        }
        else {
            this.setState({
                    editMode: true,
                }
            )
        }
    }

    render() {
        return (
            <div className={"EditPage"}>
                <Header/>

                {this.state.editMode ?
                    <h1>
                        Drücken Sie Bestätigen, um Ihre Änderungen abzuschicken
                    </h1> :
                    <h1>
                        Drücken Sie Löschen, um die Frage endgültig zu Löschen
                    </h1>
                }

                <form onSubmit={this.handleSubmit}>
                    <Grid id={"formGrid"}>
                        <Row>
                            <Col xs={12} md={12}>
                                <div className={"input-group"}>
                                    <span className={"input-group-addon"} id={"question"}>Frage </span>
                                    <input type={"text"} className={"form-control"} aria-describedby="question"
                                           value={this.state.question} onChange={this.handleChangeQuestion}/>
                                </div>
                            </Col>
                        </Row>

                        <Row>
                            <Col xs={12} md={12}>
                                <div className={"input-group"}>
                                    <span className={"input-group-addon"} id={"answer1"}>Antwort 1 </span>
                                    <input type={"text"} className={"form-control"} aria-describedby="answer1"
                                           value={this.state.answer1} onChange={this.handleChangeAnswer1}/>
                                </div>
                            </Col>
                        </Row>

                        <Row>
                            <Col xs={12} md={12}>
                                <div className={"input-group"}>
                                    <span className={"input-group-addon"} id={"answer2"}>Antwort 2 </span>
                                    <input type={"text"} className={"form-control"} aria-describedby="answer2"
                                           value={this.state.answer2} onChange={this.handleChangeAnswer2}/>
                                </div>
                            </Col>
                        </Row>

                        <Row>
                            <Col xs={12} md={12}>
                                <div className={"input-group"}>
                                    <span className={"input-group-addon"} id={"answer3"}>Antwort 3 </span>
                                    <input type={"text"} className={"form-control"} aria-describedby="answer3"
                                           value={this.state.answer3} onChange={this.handleChangeAnswer3}/>
                                </div>
                            </Col>
                        </Row>

                        <Row>
                            <Col xs={12} md={12}>
                                <div className={"input-group"}>
                                    <span className={"input-group-addon"} id={"correctAnswer"}>Korrekte Antwort </span>
                                    <input type={"text"} className={"form-control"}
                                           aria-describedby="correctAnswer" value={this.state.correctAnswer}
                                           onChange={this.handleChangeCorrectAnswer}/>
                                </div>
                            </Col>
                        </Row>

                        <Row>
                            <Col xs={12} md={12}>
                                {
                                    this.state.editMode ?
                                        <input type={"submit"} id={"submit"} value={"Bestätigen"}/>
                                        : <input type={"submit"} id={"submit"} value={"Löschen"}/>
                                }
                            </Col>
                        </Row>

                    </Grid>
                </form>

                <Grid>
                    <Row>
                        <Col xs={12} md={12}>
                            {
                                this.state.editMode ?
                                    <div id={"toggleButton"}>
                                        <div className={"alert alert-warning"} role={"alert"}>
                                            Aktueller Modus: <strong>editieren</strong>
                                        </div>
                                        <Button id={"toggleMode"} bsStyle={"warning"} bsSize={"large"}
                                                onClick={() => this.toggleMode()}>
                                            Editiermodus wechseln
                                        </Button>
                                    </div>
                                    :
                                    <div id={"toggleButton"}>
                                        <div className={"alert alert-danger"} role={"alert"}>
                                            Aktueller Modus: <strong>löschen</strong>
                                        </div>
                                        <Button id={"toggleMode"} bsStyle={"danger"}
                                                onClick={() => this.toggleMode()} bsSize={"large"}>
                                            Editiermodus wechseln
                                        </Button>
                                    </div>
                            }
                        </Col>
                    </Row>

                    <Row>
                        <Col xs={12} md={12}>
                            <Button id={"returnToQuiz"} bsStyle={"primary"} onClick={() => this.returnToQuiz()}
                                    bsSize={"large"}>
                                Kehre zum Quiz zurück
                            </Button>
                        </Col>
                    </Row>
                </Grid>

            </div>
        );
    }
}

export default EditPage;